List getCatJson() {
  return [
    {
      "url": "https://www.siamesecatspot.com/wp-content/uploads/2012/07/stark1.jpg",
      "type": "Siamese Lynx Point",
      "description": "Blend of a Siamese and Tabby cat. They are talkative like a Siamese but less aggressive. Fun fact, my cat is a Siamese Lynx Point :)"
    },
    {
      "url": "https://www.petmd.com/sites/default/files/styles/article_image/public/white-persian-cats-picture-id637190306.jpg?itok=UZI3pQwD",
      "type": "Persian",
      "description": "Persians are quiet and calm mannered. They are chubby yet sturdy and have a lot of fur."
    },
    {
      "url": "https://nutrisourcepetfoods.com/wp-content/uploads/2020/03/shutterstock_519779122-1000x600sfw-1.jpg",
      "type": "Maine Coon",
      "description": "Maine Coons are very large cats with bushy tails. Despite their size and appearance, they are very friendly."
    },
    {
      "url": "https://s36537.pcdn.co/wp-content/uploads/2018/01/Ragdoll-cat-on-blue-backdrop-.jpg.optimal.jpg",
      "type": "Ragdoll",
      "description": "Ragdolls are fluffy blue eyed cats that are smart and playful. They can even learn tricks and certain behaviors."
    },
    {
      "url": "https://vetstreet-brightspot.s3.amazonaws.com/9c/70fdd0a27c11e087a80050568d634f/file/American-Shorthair-4-645mk062211.jpg",
      "type": "American Shorthair",
      "description": "American Shorthairs aka Tabby cats are very adaptable and good natured. They are usually good as barn and working cats."
    },
    {
      "url": "https://azure.wgp-cdn.co.uk/app-yourcat/posts/siberian1.jpg?&width=480&height=480&bgcolor=ffffff&mode=crop",
      "type": "Siberian",
      "description": "Siberians are very affectionate and can be good lap cats. They are very social and get along well with other cats and children."
    },
    {
      "url": "https://sphynxforsale.club/wp-content/uploads/2019/04/20-1.jpg",
      "type": "Sphynx",
      "description": "Sphynx cats are usually pretty hairless. As they do not have much fur, they often seek out warmth from others."
    },
  ];
}