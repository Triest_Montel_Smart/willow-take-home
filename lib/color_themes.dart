import 'dart:ui';

import 'package:flutter/material.dart';

class ColorTheme {

  static ColorTheme instance;

  bool isDark;
  Color bacgroundColor;
  Color cardColor;
  Color textColor;
  Color dotColor;
  Color activeDotColor;

  ColorTheme.setColorTheme(final bool isDark) {
    instance = this;
    this.isDark = isDark;
    bacgroundColor = returnThemedColor(Color(0xFF212121), Color(0xFFE3DDDE));
    cardColor = returnThemedColor(Color(0xFF303030), Colors.white);
    textColor = returnThemedColor(Colors.grey, Colors.black);
    dotColor = returnThemedColor(Color(0xFF7D7C7B), Colors.grey);
    activeDotColor = returnThemedColor(Color(0xFF555555), Colors.black54);
  }

  Color returnThemedColor(final Color darkColor, final Color lightColor) {
    return (isDark) ? darkColor : lightColor;
  }
}