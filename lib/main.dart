import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:provider/provider.dart';
import 'package:willow_take_home/cat_json.dart';
import 'package:willow_take_home/main_view_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return MaterialApp(
      title: 'Willow Take Home',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Willow Take Home'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(final BuildContext context) {
    var catJson = getCatJson();
    return ChangeNotifierProvider<MainViewModel>(
      create: (context) => MainViewModel(context),
      child: Consumer<MainViewModel>(
        builder: (context, viewModel, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
            ),
            body: Center(
              child: Container(
                color: viewModel.colorTheme.bacgroundColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                        child: Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text("Cats",
                          style: TextStyle(color: Colors.purple, fontSize: 32)),
                    )),
                    Expanded(
                      child: PageIndicatorContainer(
                        key: UniqueKey(),
                        child: PageView.builder(
                            itemCount: 7,
                            itemBuilder: (context, position) {
                              return Container(
                                padding: EdgeInsets.fromLTRB(16, 16, 16, 96),
                                child: Container(
                                    height: 480,
                                    width: double.infinity,
                                    child: Card(
                                        color: viewModel.colorTheme.cardColor,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Container(
                                              width: double.infinity,
                                              height: 240,
                                              child: CachedNetworkImage(
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) =>
                                                      Center(
                                                          child: SizedBox(
                                                              width: 40,
                                                              height: 40,
                                                              child: CircularProgressIndicator(
                                                                  color: Colors
                                                                      .purple))),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Icon(Icons.error),
                                                  imageUrl: jsonDecode(json
                                                      .encode(catJson[position]
                                                          ["url"]))),
                                            ),
                                            Center(
                                                child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 32),
                                              child: Text(
                                                  jsonDecode(json.encode(
                                                      catJson[position]
                                                          ["type"])),
                                                  style: TextStyle(
                                                      color: viewModel.colorTheme.textColor,
                                                      fontSize: 24)),
                                            )),
                                            Center(
                                                child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      16, 32, 16, 32),
                                              child: Text(
                                                  jsonDecode(json.encode(
                                                      catJson[position]
                                                          ["description"])),
                                                  style: TextStyle(
                                                      color: viewModel.colorTheme.textColor,
                                                      fontSize: 20)),
                                            ))
                                          ],
                                        ))),
                              );
                            }),
                        align: IndicatorAlign.bottom,
                        length: 7,
                        indicatorSpace: 8,
                        padding: EdgeInsets.only(bottom: 60),
                        indicatorColor: viewModel.colorTheme.dotColor,
                        indicatorSelectorColor: viewModel.colorTheme.activeDotColor,
                        shape: IndicatorShape.circle(size: 8),
                      ),
                    )
                  ],
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: ()=> viewModel.toggleDisplayMode(),
              tooltip: 'Display Mode',
              child: Icon(viewModel.colorTheme.isDark ? Icons.wb_sunny : Icons.dark_mode),
            ), // This trailing comma makes auto-formatting nicer for build methods.
          );
        },
      ),
    );
  }
}
