import 'package:flutter/cupertino.dart';
import 'package:willow_take_home/color_themes.dart';

class MainViewModel extends ChangeNotifier {
  ColorTheme colorTheme;

  MainViewModel(final BuildContext context) {
    colorTheme = ColorTheme.setColorTheme(false);
  }

  void toggleDisplayMode() {
    colorTheme = ColorTheme.setColorTheme(!ColorTheme.instance.isDark);
    notifyListeners();
  }
}